package com.company;

public class Department {
    private int departmentId;
    private String name;

    Department (int departmentId, String name){
        this.departmentId = departmentId;
        this.name = name;
    }

    Department (String name){
        this.name = name;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDepartmentId(){
        return departmentId;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "departmentId: " + departmentId + ", name: " + name;
    }
}
