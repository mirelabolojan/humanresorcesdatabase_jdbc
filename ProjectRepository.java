package com.company;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    public static List<Project> findAllProject() {
        List<Project> projects = new ArrayList<Project>();

        try {
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("select * from project");

            while (resultSet.next()) {
                Integer projectId = resultSet.getInt("projectId");
                String description = resultSet.getString("descriptionn");

                Project project = new Project(projectId, description);

                projects.add(project);
            }

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return projects;
    }

    public static Project findProjectById(Integer projId) {
        Project project = null;
        try {
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            PreparedStatement preparedStatement = connection.prepareStatement("select * from project where projectId = ?");
            preparedStatement.setInt(1, projId);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Integer projectId = resultSet.getInt("projectId");
                String description = resultSet.getString("descriptionn");

                project = new Project(projectId, description);
            }

            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return project;
    }

    public static void deleteProjectById (Integer projId) {
        try {
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            PreparedStatement preparedStatement = connection.prepareStatement("delete from project where projectId = ?");
            preparedStatement.setInt(1, projId);
            preparedStatement.executeUpdate();

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static void saveProject(Project project) {
        try {
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            PreparedStatement preparedStatement = connection.prepareStatement("insert into project (descriptionn) values (?)");
            preparedStatement.setString(1, project.getDescription());
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static void updateProject (Project project) {
        try {
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            PreparedStatement preparedStatement = connection.prepareStatement("update project set descriptionn = ? where projectId = ?");
            preparedStatement.setString(1,project.getDescription());
            preparedStatement.setInt(2,project.getProjectId());
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static List<Project> findProjectByName (String name) {
        List<Project> projects = new ArrayList<Project>();

        try {
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            PreparedStatement preparedStatement = connection.prepareStatement("select * from project where descriptionn = ?");
            preparedStatement.setString(1, name);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Integer projectId = resultSet.getInt("projectId");
                String description = resultSet.getString("descriptionn");

                Project project = new Project(projectId,description);
                projects.add(project);
            }

            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        return projects;
    }
}
