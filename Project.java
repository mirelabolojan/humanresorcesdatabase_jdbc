package com.company;

public class Project {
    private int projectId;
    private String description;

    Project(int projectId, String description) {
        this.projectId = projectId;
        this.description = description;
    }

    Project(String description) {
        this.description = description;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProjectId() {
        return projectId;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "projectId: " + projectId + ", description: " + description;
    }
}
