create database humanResources;

create table employees(employeeId int, firstName varchar(20), lastName varchar(20), dateOfBirth date, postalAddress varchar(50));

alter table employees add (phoneNumber varchar(12), email varchar(10), salary int);

alter table employees drop column postalAddress;

alter table employees modify employeeId int auto_increment primary key;

insert into employees(firstName, lastName, dateOfBirth, phoneNumber, email, salary) values 
('Cristina', 'Vacaru', '1992-10-09', '0724439144', 'c@mail.com', '5000'),
('Mirela', 'Bolojan', '1993-06-16', '0749674029', 'm@mail.com', '6000'),
('Denisa', 'Sabau', '1999-03-01', '0754889552', 'd@gmil.com', '4000'),
('Denis', 'Bolojan', '1998-02-26', '0724439144','b@mail.com', '6500'),
('Sorina', 'Butnariu', '1997-07-23', '0754558663', 's@gmil.com', '3500');

update employees set managerId = 2  where employeeId in (1,3,4);

select * from employees where not lastName= 'Bolojan';
select count(*) from employees;
select * from employees where lastName like '%n';

create table departments(departmentId int primary key not null auto_increment, name varchar(20) not null);

insert into departments(name) values('HR'), ('Finance'), ('IT'), ('Securit'), ('Security');

delete from departments where departmentId = 5;

update departments set name = 'Sanitary' where departmentId = 4;

alter table employees add column (departmentId int);

update employees set departmentId = 2 where employeeId = 4;

alter table employees add constraint fk_departments_employees foreign key (departmentId) references departments (departmentId);

delete from employees where employeeId = 7;

alter table employees add column (managerId int);
alter table employees add constraint fk_manager_employees foreign key (managerId) references employees(employeeId);

create table project (projectId int primary key auto_increment not null, descriptionn text);

create table employeesProjects (employeesProjectId int primary key auto_increment, employeeId int, projectId int,
constraint fk_employeesProject_employee foreign key (employeeId) references employees (employeeId),
constraint fk_employeesProject_project foreign key (projectId) references project (projectId));

insert into project (descriptionn) values ('Python - Cinema Web App'), ('Java - Fitness Web App');
insert into project (descriptionn) values ('Java - Online Store');

insert into employeesprojects (employeeId,projectId) values ('2','1'), ('3','1'), ('3','2'), ('1','2');

select * from project where descriptionn like ('Java%');

select distinct lastName from employees;

select count(distinct lastName) from employees;

select * from employees where lastName in ('Bolojan', 'Sabau');
select * from employees where not lastName = 'Bolojan';
select * from employees order by lastName desc, salary desc;
select * from employees limit 2;
select min(salary) as minSalary from employees;
select * from employees where departmentId in (select departmentId from departments where name = 'Finance');
select  concat(firstName , ' ', lastName) AS Namee, dateOfBirth as birthDate  from employees;
select concat(firstName, ' ', lastName) as namee, d.name as nameOfDepartment from employees as e, departments as d where e.departmentId = d.departmentId and d.name = 'Finance';
select * from employees inner join departments on employees.departmentId = departments.departmentId and departments.name = 'HR';
select * from employees inner join departments on employees.departmentId = departments.departmentId order by employees.lastName;
select departments.departmentId, lastName, firstName, count(employees.lastName) as nrOfLastName from employees inner join 
departments on employees.departmentId = departments.departmentId group by lastName;

select * from project;
select employeeId, firstName, lastName, dateOfBirth from employees;
select employeeId, firstName, lastName, dateOfBirth from employees where firstName like 'S%' or lastName like 'S%';
select * from employees where departmentId is null;
select employeeId, firstName, lastName, dateOfBirth, name from employees inner join departments on employees.departmentId = departments.departmentId order by name;
