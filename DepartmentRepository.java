package com.company;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentRepository {

    public static List<Department> findAllDepartments() {
        List<Department> departments = new ArrayList<Department>();

        try{
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from departments");

            while (resultSet.next()) {
                Integer departmentId = resultSet.getInt("departmentId");
                String name = resultSet.getString("name");

                Department department = new Department(departmentId, name);
                departments.add(department);
            }

            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return departments;
    }

    public static Department findDepartmentById(Integer depId){
        Department department = null;
        try{
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            PreparedStatement preparedStatement = connection.prepareStatement("Select * from departments where departmentId = ?");
            preparedStatement.setInt(1, depId);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Integer departmentId = resultSet.getInt("departmentId");
                String name = resultSet.getString("name");

                department = new Department(departmentId, name);
            }

        } catch (SQLException sqlException ){
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return department;
    }

    public static void deleteDepartmentById(Integer departmId) {
        try{
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            PreparedStatement preparedStatement = connection.prepareStatement("Delete from departments where departmentId = ?");
            preparedStatement.setInt(1, departmId);

            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static void saveDepartment(Department department) {
        try {
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            PreparedStatement preparedStatement = connection.prepareStatement("Insert into departments (name) values (?)");
            preparedStatement.setString(1, department.getName());
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static void updateDepartment (Department department){
        try {
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            PreparedStatement preparedStatement = connection.prepareStatement("update departments set name = ? where departmentId = ?");

            preparedStatement.setString(1,department.getName());
            preparedStatement.setInt(2, department.getDepartmentId());
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static List<Department> findDepartmentByName(String name){
        List<Department> departments = new ArrayList<Department>();

        try{
            Connection connection = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST, DatabaseUtils.DATABASE_USERNAME, DatabaseUtils.DATABASE_PASSWORD);

            PreparedStatement preparedStatement = connection.prepareStatement("Select * from departments where name = ?");
            preparedStatement.setString(1,name);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Integer departmentId = resultSet.getInt("departmentId");
                String departmentName = resultSet.getString("name");

                Department department = new Department(departmentId, departmentName);

                departments.add(department);
            }

            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return departments;
    }
}
