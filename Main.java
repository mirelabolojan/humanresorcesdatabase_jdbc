package com.company;

import static com.company.DepartmentRepository.*;
import static com.company.ProjectRepository.*;

public class Main {
    public static void main(String[] args) {
//        deleteDepartmentById(6);
//        saveDepartment(new Department("Sanitary"));
//        updateDepartment(new Department(7, "Sanitary"));
//        System.out.println(findAllDepartments());
//        System.out.println(findDepartmentById(3));
//        System.out.println(findDepartmentByName("Finance"));

//        deleteProjectById(3);
//        saveProject(new Project("Java - Online Shop"));
//        updateProject(new Project(4, "Java - Online Store"));
        System.out.println(findAllProject());
        System.out.println(findProjectById(2));
        System.out.println(findProjectByName("Java - Online Store"));
    }
}
